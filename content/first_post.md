+++
title = "Air Quality in the Time of Coronavirus"
date = 2020-04-17

[taxonomies]
categories = ["News", "Thoughts"]
tags = ["COVID19","Urban Air Pollution"]
+++

You don't have to look far to be reminded that we're living through exceptional times. Sure, all one has to do is have a look out the window at the eerie world outside.    Air quality, too, has not failed to grab its fair share of headlines during this time. Social media feeds and news sites regularly feature satellite air pollution imagery and photographs of urban skylines with unexpected crystalline skies.

<!-- more -->

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
in culpa qui officia deserunt mollit anim id est laborum.
