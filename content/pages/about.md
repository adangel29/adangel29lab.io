+++
title = "About"
path = "about"
template = "about.html"
+++


My name is Alexander Dangel. I am a researcher at the [Research Center for Environmental Economics](https://www.uni-heidelberg.de/fakultaeten/wiso/awi/professuren/umwelt/rcee.html) 
at the [Ruprecht-Karls-Univeristät Heidelberg](https://www.uni-heidelberg.de/en) in Germany.

This blog collects my thoughts, research, and news on all things air quality. I'm particularly interested in air pollution warning systems and decentralized monitoring networks.  

You can best reach me via [email](mailto:alexander.dangel@awi.uni-heidelberg.de).

